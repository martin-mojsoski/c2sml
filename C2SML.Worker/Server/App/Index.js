window.onerror = function(error)
{
    PrintError(error);
};

function work()
{
    var request = new XMLHttpRequest();
    request.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            WorkersParse(JSON.parse(this.responseText));
        }
    };
    request.open("GET", "index.php?action=get_workers", true);
    request.send();
}

function nojs()
{
    document.location = "index.php";
}

function result()
{
    var worker = new URL(document.location).searchParams.get("name");
    var id = new URL(document.location).searchParams.get("id");
    var request = new XMLHttpRequest();
    request.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            var result = JSON.parse(this.responseText);
            if (result["error"] === "2")
            {
                document.getElementById("_result").value = "Not yet done! Refreshing in 5 seconds.";
                setTimeout(function() { location.reload(); }, 5000);
            }
            else
            {
                GenerateResultForm(result);
            }
        }
    };
    request.open("GET", "index.php?action=check_finished&name=" + worker + "&id=" + id, true);
    request.send();
}

function error()
{
    document.getElementById("_error").value = new URL(document.location).searchParams.get("text");
}

function AssignWork(worker, work)
{
    var request = new XMLHttpRequest();
    var result = null;
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200)
        {
            result = JSON.parse(this.responseText);
        }
    };
    request.open("POST", "index.php?action=assign_work", false);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    request.send(encodeURI("name=" + worker + "&work=" + work));
    return result;
}

function WorkersParse(arr)
{
    var a = Array();
    for (var i = 0; i < arr["workers"].length; i++)
    {
        a[i] = arr["workers"][i];
        a[i] = a[i].substring(8, a[i].length - 5);
        var x = document.getElementById("name");
    }
    ConstructPage(a);
}

function TryAgain()
{
    document.location = new URL(document.location).searchParams.get("url");
}

function AddWorker(worker)
{
    var request = new XMLHttpRequest();
    request.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            var option = new Option();
            var result = JSON.parse(this.responseText);
            if (result["ago"] > 120) return;
            option.text = (result["ago"] < 2 ? "ready_" : (result["ago"] < 12 ? "loaded_" : "down_"))
                + worker.toLowerCase() + "_timeout_" + result["ago"];
            option.value = worker;
            document.getElementById("_workers").add(option);
        }
    };
    request.open("GET", "index.php?action=worker_timeout&name=" + worker, false);
    request.send();
}

function PrintError(message)
{
    document.location = "Error.html?text="+ encodeURI(message) + "&url=" + document.location;
}

function WaitForResult(worker, id)
{
    document.location = "Result.html?name=" + worker + "&id=" + id;
}

function SubmitClick()
{
    var workers = document.getElementById("_workers");
    var worker = workers[workers.selectedIndex].value;
    var work = document.getElementById("_work").value;
    var result = AssignWork(worker, work);
    if (result["error"] === "0")
    {
        WaitForResult(worker, result["id"]);
    }
    else 
    {
        PrintError("Error while sending request: " + result["error"]);
    }
}

function ConstructPage(workers)
{
    for (var i = 0; i  < workers.length; i++)
    {
        AddWorker(workers[i]);
    }
    if (document.getElementById("_workers").length === 0)
    {
        PrintError("No available workers!");
        return;
    }

    document.getElementById("_work").value = "Label Job.Work\n" +
        "Env.Write $WL_FOUNDJOB $$n\n" +
        "Server.SendResult $Server.URL $Server.Id\n" +
        "Thread.Sleep 500\n" +
        "Return";
    
    document.getElementById("_submit").innerText = "Request Work\n";
}

function SelectVar()
{
    var sel = document.getElementById("_name");
    document.getElementById("_result").value = sel[sel.selectedIndex].value;
}

function SortByKey(dict)
{

    var sorted = [];
    
    for(var key in dict)
    {
        sorted[sorted.length] = key;
    }
    
    sorted.sort();

    var tempDict = {};
    for(var i = 0; i < sorted.length; i++)
    {
        tempDict[sorted[i]] = dict[sorted[i]];
    }

    return tempDict;
}

function GenerateResultForm(result)
{
    result = SortByKey(result);
    Object.keys(result).forEach(function(key)
    {
        var hide = ["error", "Out", "Error", "Def"];
        if (key.substring(0, 2) === "l:") return;
        if (key.substring(0, 1) === "$") return;
        if (key.substring(key.length - 5, key.length) === ".conf") return;
        if (key.substring(0, 1) === "_") return;
        if (key.substring(0, 3) === "WL_") return;
        if (hide.includes(key)) return;
        var option = new Option();
        option.text = key;
        option.value = result[key];
        document.getElementById("_name").add(option);
    });
    SelectVar();
}