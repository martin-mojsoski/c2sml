<?php
$client = array();
header("Content-Type: application/json; charset=UTF-8");

if ($_GET["action"] != "")
{
    switch($_GET["action"])
    {
        case "worker_timeout":
            $client["last_seen"] = (int)file_get_contents("Workers/" . $_GET["name"] . ".work");
            $client["ago"] = time() - $client["last_seen"];
        break;
        case "get_workers":
            $client["workers"] = glob("Workers/*.work");
            $client["error"] = "0";
            break;
        case "assign_work":
            if (!file_exists("Workers/" . $_POST["name"] . ".work"))
            {
                $client["error"] = "2";
            }
            else
            {
                $client["error"] = "0";
                $client["id"] = time();
                while(file_exists($_POST["name"] . ".work." . $client["id"], $_POST["work"]))
                {
                    $client["id"] += "a";
                }
                file_put_contents("Workers/" . $_POST["name"] . ".work." . $client["id"], $_POST["work"]);
            }
            break;
        case "finish_work":
            $filepath = $_FILES["file"]["tmp_name"];
            move_uploaded_file($filepath,"Workers/" . $_GET["name"] . ".work_done." . $_GET["id"]);
            $client["error"] = 0;
            break;
        case "check_finished":
            if (file_exists("Workers/" . $_GET["name"] . ".work_done." . $_GET["id"]))
            {
                $client = json_decode(file_get_contents("Workers/" . $_GET["name"] . ".work_done." . $_GET["id"]), true);
                $client["error"] = "0";
            }
            else
            {
                $client["error"] = "2";
            }
            break;
        case "info":
            $client["version"] = "0.0.2";
            $client["error"] = "0";
            break;
        case "register":
            file_put_contents("Workers/" . $_GET["name"] . ".work", time());
            $client["error"] = "0";
            break;
        case "get_work":
            if (!file_exists("Workers/" . $_GET["name"] . ".work"))
            {
                $client["error"] = "-1";
            }
            else
            {
                $job = glob("Workers/" . $_GET["name"] . ".work.*");
                if (count($job) < 1)
                {
                    $client["job"] = "Label Job.Work\nThread.Sleep 500\nReturn";
                    $client["error"] = "0";
                }
                else
                {
                    $client["job"] = file_get_contents($job[0]);
                    $client["id"] = substr($job[0], -10);
                    unlink($job[0]);
                    $client["error"] = "0";
                }
                $client["error"] = "0";
            }
            file_put_contents("Workers/" . $_GET["name"] . ".work", time());
            break;
        default:
            $client["error"] = "1";
            break;
    }
}
else
{
    $client["error"] = "-1";
    header("Location: Work.html");
}

echo json_encode($client);
?>