using System.Collections.Concurrent;

namespace C2SML.Worker
{
    internal static class Locks
    {
        private static readonly ConcurrentDictionary<string, object> LockDictionary = new ConcurrentDictionary<string, object>();

        internal static object Lock(string name) => LockDictionary.GetOrAdd(name, _ => new object());
    }
}