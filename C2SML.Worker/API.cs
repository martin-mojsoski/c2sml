using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace C2SML.Worker
{
    internal partial class Program
    {
        internal void ApiCall(string method, params string[] args) => _api[method].Invoke(args);
        private void Set(string key, string value) => ApiCall("Set", key, value);
        
        private void InitApi()
        {
            _events = new Dictionary<string, Action<string>>()
            {
                {"Errors.OnError", delegate {}},
                {"Errors.OnWarn", delegate {}}
            };
            _api = new Dictionary<string, Action<string[]>>
            {
                // File I/O
                {"File.Exists", args => Set(args[0], File.Exists(args[1]).ToString())},
                {"File.Write", args => File.WriteAllText(args[0], args[1])},
                {"File.Delete", args => File.Delete(args[0])},
                {"File.Append", args => File.AppendAllText(args[0], args[1])},
                {"File.Read", args => Set(args[0], File.ReadAllText(args[1]))},
                {"Directory.Get", args => Set(args[0], JsonConvert.SerializeObject(Directory.GetFileSystemEntries(args[1])))},
                {"Directory.Exists", args => Set(args[0], Directory.Exists(args[1]).ToString())},
                {"Directory.Create", args => Directory.CreateDirectory(args[0])},
                {"Directory.Delete", args => Directory.Delete(args[0])},

                // Debug
                {"Debug.GetErrors", args =>
                    {
                        string env = args[0], size = args[1];
                        var list = new List<string>();
                        switch (size)
                        {
                            case "All":
                                ErrorHandler.Errors.ForEach(e => list.Add(e.ToString()));
                                ErrorHandler.Errors.Clear();
                                break;
                            case "Warn":
                                ErrorHandler.Warnings.ForEach(e => list.Add(e.ToString()));
                                ErrorHandler.Warnings.Clear();
                                break;
                            default:
                            {
                                if (int.TryParse(size, out var x))
                                {
                                    for (var i = 0; i < x && i < ErrorHandler.Errors.Count; i++)
                                    {
                                        list.Add(ErrorHandler.Errors[i].ToString());
                                        ErrorHandler.Errors.RemoveAt(i);
                                    }
                                }
                                else if (list.Count > 0)
                                {
                                    list.Add(ErrorHandler.Errors[0].ToString());
                                    ErrorHandler.Errors.RemoveAt(0);
                                }
                                break;
                            }
                        }
                        Set(env, JsonConvert.SerializeObject(list));
                    }
                },
                {"Debug.Throw", args => throw new Error(args[0])},
                {"Debug.Warn", args => ErrorHandler.AddWarning(new Warning(args[0]))},
                
                // HTTP
                {"HTTP.Get", args => Set(args[0], _web.DownloadString(args[1]).Trim())},
                {"HTTP.Upload", args => Set(args[0], Encoding.UTF8.GetString(_web.UploadFile(args[1], "POST", args[2])))},
                {"HTTP.Post", args => Set(args[0], _web.UploadString(args[1], args[2]).Trim())},
                
                // Env
                {"Env.Execute", args =>
                    {
                        string env = args[0],
                            envError = args[1],
                            command = args[2],
                            arguments = args[3],
                            input = args[4];
                        using (var process = new Process
                        {
                            StartInfo =
                            {
                                FileName = command,
                                Arguments = arguments,
                                UseShellExecute = false,
                                CreateNoWindow = true,
                                RedirectStandardInput = true,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true
                            }
                        })
                        {
                            process.Start();
                            process.StandardInput.Write(input);
                            Set(env, process.StandardOutput.ReadToEnd().Trim());
                            Set(envError, process.StandardError.ReadToEnd().Trim());
                            process.WaitForExit(10000);
                        }
                    }
                },
                {"Env.Write", args => Console.Write(args.Aggregate((x,y) => x+" "+y))},
                {"Env.Read", args => Set(args[0], Console.ReadLine())},
                {"Env.Exit", args => Environment.Exit(args.Length == 0 ? 0 : int.Parse(args[0]))},
                
                // Math
                {"Math.Add", args => Set(args[0], (float.Parse(args[1]) + float.Parse(args[2])).ToString(CultureInfo.CurrentCulture))},
                {"Math.Mul", args => Set(args[0], (float.Parse(args[1]) * float.Parse(args[2])).ToString(CultureInfo.CurrentCulture))},
                {"Math.Take", args => Set(args[0], (float.Parse(args[1]) - float.Parse(args[2])).ToString(CultureInfo.CurrentCulture))},
                {"Math.Div", args => Set(args[0], (float.Parse(args[1]) / float.Parse(args[2])).ToString(CultureInfo.CurrentCulture))},
                {"Math.IntDiv", args => Set(args[0], (int.Parse(args[1]) / int.Parse(args[2])).ToString(CultureInfo.CurrentCulture))},
                {"Math.Sqrt", args => Set(args[0], Math.Sqrt(int.Parse(args[1])).ToString(CultureInfo.CurrentCulture))},
                {"Math.Int", args => Set(args[0], ((int)Math.Round(float.Parse(args[1]))).ToString(CultureInfo.CurrentCulture))},

                // Var
                {"Var.IsDefined", args =>
                    {
                        var x = "";
                        lock (this)
                        {
                            x = _vars.ContainsKey(args[1]).ToString();
                        }
                        Set(args[0], x);
                    }
                },
                {"Var.Append", args => Set(args[0], args[1] + args[2])},
                {"Var.Expand", args =>
                    {
                        var x = "";
                        lock (this)
                        {
                             x = _vars[args[1]];
                        }
                        Set(args[0], x);
                    }
                },
                {"Var.Trim", args => Set(args[0], args[1].Trim())},
                {"Var.Split", args => Set(args[0], JsonConvert.SerializeObject(args[1].Split(args[2])))},
                {"Var.Lex", args => Set(args[0],JsonConvert.SerializeObject(Lex(args[1])))},
                {"Var.Substring", args => Set(args[0], args[1].Substring(int.Parse(args[2]), int.Parse(args[3])))},
                {"Var.Length", args => Set(args[0], args[1].Length.ToString())},

                // Array
                {"Array.Set", args =>
                    {
                        var x = "";
                        lock (this)
                        {
                            x = JsonConvert.SerializeObject(_vars);
                        }
                        Set(args[0], x);
                    }
                },
                {"Array.Get", args => Set(args[0], TryGenerateJArray(args[1])[args[2]].ToString())},
                {"Array.Has", args => Set(args[0], TryGenerateJArray(args[1]).ToObject<Dictionary<string,string>>().ContainsKey(args[2]).ToString())},
                {"Array.Index", args => Set(args[0], TryGenerateJArray(args[1])[int.Parse(args[2])].ToString())},
                {"Array.Count", args => Set(args[0], TryGenerateJArray(args[1]).Count().ToString())},
                {"Array.ForEach", args =>
                    {
                        string env = args[0], arr = args[1], eval = args[2];
                        foreach (var item in TryGenerateJArray(arr))
                        {
                            if (item == null) return;
                            try
                            {
                                Set(env, item.ToString());
                                ApiCall("Eval", eval);
                            }
                            catch (Exception e)
                            {
                                ErrorHandler.AddWarning(e);
                            }
                        }
                    }
                },
                // Thread
                {"Thread.Lock", args =>
                    {
                        lock (Locks.Lock(args[0]))
                        {
                            ApiCall("Eval", args[1]);
                        }
                    }
                },
                {"Thread.Start", args =>
                        new Thread(() => new Program {_api = _api, _vars = _vars, _events = _events}
                            .ApiCall("Call", args[0])).Start()
                },
                {"Thread.Sleep", args => Thread.Sleep(int.Parse(args[0]))},
                
                // Event
                {"Event.Set", args =>
                    {
                        if (!_events.ContainsKey(args[0]))
                        {
                            _events.Add(args[0], delegate {});
                        }
                        _events[args[0]] += arg => ApiCall(args[1], arg);
                    }
                },
                {"Event.Invoke", args => _events[args[0]].Invoke(args[1])},
                
                // Loops and functions
                {"Function", args =>
                    {
                        var name = args[0];
                        var tmp = args.ToList();
                        tmp.RemoveAt(0);
                        
                        if (tmp.Any(x => !x.StartsWith('_')))
                            ApiCall("Debug.Warn",$"An argument in the function {name} didn't start with a '_'.");
                        
                        args = tmp.ToArray();
                        
                        ApiCall("Label", name);
                        
                        _api.Add(name, x =>
                        {
                            for (var i = 0; i < args.Length; i++)
                                Set(args[i], x[i]);
                            
                            ApiCall("Call", name);
                            
                            foreach (var t in args)
                                ApiCall("Unset", t);
                        });

                    }
                },
                {"Call", args =>
                    {
                        var env = args[0];
                        string[] loc;
                        lock (this)
                        {
                            if (!_vars.ContainsKey("l:" + env)) throw new Error($"Cannot find label '{env}'.");
                            loc = _vars["l:" + env].Split('|');
                        }
                        if (loc.Length != 2) throw new Error("Invalid label content.");
                        _ints.Push(_i);
                        _i = int.Parse(loc[0]);
                        _files.Push(_file);
                        _file = loc[1];
                        Run();
                        _file = _files.Pop();
                    }
                },
                {"Goto", args =>
                    {
                        lock (this)
                        {
                            var env = args[0];
                            if (!_vars.ContainsKey("l:" + env)) throw new Error($"Cannot find label '{env}'.");
                            var loc = _vars["l:" + env].Split('|');
                            if (loc.Length != 2) throw new Error("Invalid label content.");
                            if (_file != loc[1])
                                throw new Error("Cannot goto a label from an other file, try calling it.");
                            _i = int.Parse(loc[0]);
                        }
                    }
                },
                {"If", args =>
                    {
                        string one = args[0], op = args[1], two = args[2], eval = args[3];
                        var should = false;
                        switch (op)
                        {
                            case "==":
                                should = one == two;
                                break;
                            case "<=":
                                should = float.Parse(one) <= float.Parse(two);
                                break;
                            case ">=":
                                should = float.Parse(one) >= float.Parse(two);
                                break;
                            case "!=":
                                should = one != two;
                                break;
                            case ">":
                                should = float.Parse(one) > float.Parse(two);
                                break;
                            case "<":
                                should = float.Parse(one) < float.Parse(two);
                                break;
                            default:
                                throw new Error($"Incorrect operation {op}.");
                        }

                        if (should) ApiCall("Eval", eval);
                    }
                },
                {"Label", args => Set("l:" + args[0], _i.ToString() + "|" + _file)},
                {"Return", _ =>
                    {
                        lock (this)
                        {
                            _i = _vars[_file].Split('\n').Length;
                        }
                    }
                },
                
                // Tied to the interpreter
                {"Load", args =>
                    {
                        if (_file != "") _files.Push(_file);
                        _file = args[0];

                        ApiCall("File.Read", _file, _file);

                        _ints.Push(_i);
                        PreRun();
                        _i = 0;
                        Run();
                        _file = _files.Pop();
                    }
                },
                {"Include", args =>
                    {
                        if (_file != "") _files.Push(_file);
                        _file = args[0];

                        ApiCall("File.Read", _file, _file);

                        _ints.Push(_i);
                        PreRun();
                        _i = _ints.Pop();
                        _file = _files.Pop();
                    }
                },
                {"Eval", args => CommandHandler(args[0])},
                                
                // Memory management
                {"Unset", args =>
                    {
                        lock (this)
                        {
                            foreach (var key in args) if (_vars.ContainsKey(key))
                            {
                                _vars.Remove(key);
                            }
                        }
                    }
                },
                {"Set", args =>
                    {
                        lock (this)
                        {
                            if (_vars.ContainsKey(args[0]))
                            {
                                _vars[args[0]] = args[1];
                            }
                            else
                            {
                                _vars.Add(args[0], args[1]);
                            }
                        }

                    }
                }
            };
        }        
    }
}