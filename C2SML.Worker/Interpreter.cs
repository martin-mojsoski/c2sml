using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace C2SML.Worker
{
    internal partial class Program
    {
        private List<string> Lex(string s)
        {
            var quotes = false;
            var tokens = new List<string> {""};
            foreach (var c in s)
            {
                if (c == '"')
                {
                    quotes = !quotes;
                }
                else if (char.IsWhiteSpace(c) && quotes == false)
                {
                    tokens.Add("");
                }
                else
                {
                    tokens[tokens.Count - 1] += c;
                }
            }

            for (var i = 0; i < tokens.Count; i++)
                if (tokens[i].StartsWith('$'))
                    if (_vars.ContainsKey(tokens[i].Substring(1)))
                        tokens[i] = _vars[tokens[i].Substring(1)];
                    else tokens[i] = string.Empty;

            return tokens;
        }
        
        private void CommandHandler(string command) =>
            Try<string,object>.Run(ExecuteCommand, command, out _);
        
        private object ExecuteCommand(string input)
        {
            var command = Lex(input);
            if (string.IsNullOrEmpty(command[0].Trim()))
                return null;
            var method = command[0];
            command.RemoveAt(0);
            ApiCall(method, command.ToArray());
            return null;
        }

        private void PreRun()
        {
            var content = _vars[_file].Split('\n');
            for(_i = 0; _i < content.Length; _i++)
            {
                if (content[_i].Trim().StartsWith('#')) continue;
                switch (content[_i].Trim().Split(' ')[0])
                {
                    case "Function":
                    case "Label":
                        CommandHandler(content[_i].Trim());
                        continue;
                    default:
                        continue;
                }
            }   
        }
        
        private void Run()
        {
            var content = _vars[_file].Split('\n');
            for(; _i < content.Length; _i++)
            {
                if (content[_i].Trim().StartsWith('#') || (content[_i].Trim().StartsWith("Function"))) continue;
                CommandHandler(content[_i].Trim());
            }
            
            if (_ints.Count != 0)
                _i = _ints.Pop();
        }
        
        private static JToken TryGenerateJArray(string json) => 
            Try<string,JToken>.Run(JToken.Parse, json, out _);
    }
}