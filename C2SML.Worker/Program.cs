﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Net;
using System.Reflection;

namespace C2SML.Worker
{
    internal partial class Program
    {
        private readonly WebClient _web = new WebClient();
        private readonly Stack<string> _files;
        private readonly Stack<int> _ints;
        
        private int _i = 0;
        private string _file = "";
        
        private Dictionary<string, Action<string[]>> _api;
        private Dictionary<string, Action<string>> _events;
        private Dictionary<string, string> _vars;
        
        internal static readonly Program Instance = new Program();
        
        private Program()
        {
            _vars = new Dictionary<string, string>
            {
                {"$NL", Environment.NewLine},
                {"$DQ", "\""},
                {"$Q", "'"},
                {"$n", "\n"},
                {"$r", "\r"},
                {"$t", "\t"},
                {"$v", "\v"},
                {"$a", "\a"},
                {"$b", "\b"},
                {"$f", "\f"},
                {"$$", "$"},
                {"$", "$"}
            };
            _ints = new Stack<int>();
            _files = new Stack<string>();
            InitApi();
        } 
        
        private static void Main(string[] args)
        {            
            foreach (var file in args)
            {
                Instance.ApiCall("Load",file);
            }
        }
    }
}