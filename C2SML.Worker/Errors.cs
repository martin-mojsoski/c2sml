using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C2SML.Worker
{
    internal static class ErrorHandler
    {
        static ErrorHandler()
        {
            Errors = new List<Exception>();
            Warnings = new List<Exception>();
        }

        internal static readonly List<Exception> Errors;
        internal static readonly List<Exception> Warnings;
        
        internal static void AddWarning(Exception e)
        {
            Warnings.Add(e);
            Program.Instance.ApiCall("Event.Invoke", "Errors.OnWarn", e.ToString());
        }
        
        internal static void Add(Exception e)
        {
            Errors.Add(e);
            Program.Instance.ApiCall("Event.Invoke", "Errors.OnError", e.ToString());
        }
    }
    
    public class Warning : Exception { public Warning(string msg) : base(msg) {} }
    
    public class Error : Exception { public Error(string msg) : base(msg) {} }
    
    public static class Try<T,Y>
        where T : class
        where Y : class
    {
        private static Y TryFunc(Func<Y> method, out bool out1)
        {
            try
            {
                out1 = true;
                return method.Invoke();
            }
            catch (Warning e)
            {
                ErrorHandler.AddWarning(e);;
                out1 = false;   
                return null;
            }
            catch (Exception e)
            {
                ErrorHandler.Add(e);
                out1 = false;
                return null;
            }
        }

        public static Y Run(Func<T, Y> method, T in1, out bool out1) => TryFunc(() => method.Invoke(in1), out out1);
    }
}
